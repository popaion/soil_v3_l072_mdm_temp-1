/*
 * common_list.c
 *
 *  Created on: Oct 22, 2020
 *      Author: esse
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common_list.h"
#include "at_parser.h"

<<<<<<< HEAD
urc_t g_urc;

LIST_T new_list(void)
{
	g_urc.urc_list = NULL;
	g_urc.count = 0;
	return NULL;

}

void destroy_list(LIST_T list)
{
	LIST_T temp_list;
	for (; !is_null(list); list = temp_list)
	{
		temp_list = list->next;
		free(list);
	}
}

LIST_T add_after(LIST_T list, const char* data)
{
	LIST_T temp_list;
	if ((temp_list = (LIST_T) malloc(sizeof(common_list_t))) == NULL)
	{
		return list;
	}
	temp_list->data = data;
	temp_list->next = list;
	return temp_list;
}

void print_all(LIST_T list)
{
	if (list == NULL)
	{
		return;
	}
	for (; !is_null(list); list = list->next)
	{
		printf("\n\r%s", list->data);
	}
}

void add_before(LIST_T list, char* new_data)
{
	if (list == NULL)
	{
		return;
	}
	LIST_T new_list = (LIST_T) malloc(sizeof(common_list_t));
	new_list->data = new_data;
	new_list->next = list->next;
	list->next = new_list;
}

LIST_T delete_first(LIST_T list)
{
	LIST_T temp = list;
	list = list->next;
	free(temp);
	return list;

}

char* get_first(LIST_T list)
{
	if (is_null(list))
	{
		return NULL;
	}
	return list->data;
}

uint8_t is_null(LIST_T list)
{
	return list == NULL;
}

uint8_t is_full(LIST_T list)
{
	return FALSE;
=======
urc_t 	g_urc;

LIST_T new_list(void){
	g_urc.urc_list = NULL;
	g_urc.count = 0;
    return NULL;

}

void destroy_list(LIST_T list){
     LIST_T temp_list;
     for(;!is_null(list); list=temp_list){
         temp_list = list->next;
         free(list);
     }
}

LIST_T add_after(LIST_T list, char* data){
    LIST_T temp_list;
    if((temp_list= (LIST_T)malloc(sizeof(common_list_t)))==NULL){
        return list;
    }
    temp_list->data = data;
    temp_list->next = list;
    return temp_list;
}

void print_all(LIST_T list){
    if(list == NULL){
        return;
    }
    for(;!is_null(list); list=list->next){
        printf("\n\r%s", list->data);
    }
}

void add_before(LIST_T list,char* new_data){
    if(list == NULL){
        return;
    }
    LIST_T new_list = (LIST_T)malloc(sizeof(common_list_t));
    new_list->data = new_data;
    new_list->next = list->next;
    list->next = new_list;
}


LIST_T delete_first(LIST_T list){
     LIST_T temp = list;
     list = list->next;
     free(temp);
     return list;

}

char* get_first(LIST_T list){
     if(is_null(list)){
         return NULL;
     }
     return list->data;
}

uint8_t is_null(LIST_T list){
    return list==NULL;
}

uint8_t is_full(LIST_T list){
    return FALSE;
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
}

//count list elements recursive
uint32_t count_elements(LIST_T list)
{
<<<<<<< HEAD
	if (list == NULL)
		return 0;

	return 1 + count_elements(list->next);
}

=======
    if (list == NULL)
        return 0;

    return 1 + count_elements(list->next);
}


>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
